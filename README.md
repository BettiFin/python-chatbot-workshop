# Baue dir einen Chatbot mit Python!

Hier findest du Code Beispiele zur Einführung in Python sowie einen einfachen Chatbot in Python.
Ein Lexikon hilft dir, die verschiedenen Konzepte in Python anhand von anschaulichen Beispielen nachzuvollziehen.

Viel Spaß bei der Programmierung!